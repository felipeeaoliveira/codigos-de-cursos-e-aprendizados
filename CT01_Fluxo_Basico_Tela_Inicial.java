package br.fleetdesk.automacao.fleetmate;



//import org.checkerframework.dataflow.qual.TerminatesExecution;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import io.github.cdimascio.dotenv.Dotenv;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


//import io.github.cdimascio.dotenv.Dotenv;

public class CT01_Fluxo_Basico_Tela_Inicial {
	Dotenv dotenv = Dotenv.load();
	Logger logger = LoggerFactory.getLogger(CT01_Fluxo_Basico_Tela_Inicial.class);
	
	@Test
	public void teste() throws InterruptedException {

		logger.info("client requested process the following list: {}", CT01_Fluxo_Basico_Tela_Inicial.class);
		System.setProperty("webdriver.chrome.driver",dotenv.get("PATH_WEBDRIVER_CHROME_DRIVER"));
		WebDriver driver = new ChromeDriver();
		driver.manage().window().setSize(new Dimension(1800, 1600));
		driver.get("http://homolog.core.fleetdesk.com.br/login");		    
		System.out.print(driver.getTitle());
		logger.debug("Starting process");
		
		
		driver.findElement(By.id("texUsuario")).click();
	    driver.findElement(By.id("texUsuario")).sendKeys("homolog@fleetdesk.com.br");
	    Thread.sleep(2000);
	    driver.findElement(By.id("pwdSenha")).click();
	    driver.findElement(By.id("pwdSenha")).sendKeys("hml123");
	    Thread.sleep(2000);
		
	    driver.findElement(By.id("btnLogin")).click();
	    Thread.sleep(3000);
	    driver.findElement(By.xpath("/html[1]/body[1]/div[2]/nav[1]/ul[1]/li[3]/ul[1]/li[1]/a[1]")).click();
	    Thread.sleep(3000);
	    //driver.findElement(By.xpath("(//a[contains(text(),'FleetMate')])")).click();
	    driver.findElement(By.linkText("FleetMate")).click();    
	    Thread.sleep(3000);
	    
		driver.quit();
		
	} 

}
