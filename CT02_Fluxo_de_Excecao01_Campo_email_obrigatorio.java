package fleetdesk.app.login;

import static org.junit.Assert.assertEquals;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.junit.Assert;
import org.junit.Before;

import com.sun.xml.internal.bind.v2.schemagen.xmlschema.List;


import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.bys.builder.AppiumByBuilder;
import io.appium.java_client.remote.MobileCapabilityType;
import jdk.nashorn.internal.runtime.options.Options;

public class CT02_Fluxo_de_Excecao01_Campo_email_obrigatorio {
	
	@Test
	
	public void realizarLogin() throws MalformedURLException {
		DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
	    desiredCapabilities.setCapability("platformName", "Android");
	    desiredCapabilities.setCapability("appium:deviceName", "emulator-5554");
	    desiredCapabilities.setCapability("appium:automationName", "uiautomator2");
	    desiredCapabilities.setCapability("appium:appPackage", "com.pr1me.fleetdesk");
	    desiredCapabilities.setCapability("appium:appActivity", "com.pr1me.fleetdesk.MainActivity");
	    //desiredCapabilities.setCapability(MobileCapabilityType.APP, "C:\\Users\\Adm\\fleetdesk-flutter\\apk\\app-debug.apk");
	    desiredCapabilities.setCapability("appium:ensureWebviewsHavePages", true);
	    desiredCapabilities.setCapability("appium:nativeWebScreenshot", true);
	    desiredCapabilities.setCapability("appium:newCommandTimeout", 10000);
	    desiredCapabilities.setCapability("appium:connectHardwareKeyboard", true); 
	    
	    
	    
	    AndroidDriver<MobileElement> driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), desiredCapabilities);
	    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	    
	    System.out.println(driver.getCapabilities());
	    
	    WebDriverWait wait = new WebDriverWait(driver, 1000);
		
	    MobileElement el1 = (MobileElement) driver.findElementByAccessibilityId("OK");
	    el1.click();	    
	    
	    WebDriverWait wait1 = new WebDriverWait(driver, 5000);
	    	
	   	     
	     // CAMPO SENHA	     
	     driver.findElementByXPath("//android.widget.EditText[@text='Informe sua senha']").click();
	     driver.findElementByXPath("//android.widget.EditText[@text='Informe sua senha']").sendKeys("12345678");
	     
	     WebDriverWait wait3 = new WebDriverWait(driver, 5000);
	     
	     //BOT�O ENTRA
	     driver.findElementByXPath("//android.widget.Button[@content-desc='Entrar']").click();
	     
	     
	
	    
	    driver.quit();
	    
	    
	    
	    
		
	}

}
