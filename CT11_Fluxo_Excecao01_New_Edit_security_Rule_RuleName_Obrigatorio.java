package br.fleetdesk.automacao.fleetmate;

//import org.checkerframework.dataflow.qual.TerminatesExecution;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import io.github.cdimascio.dotenv.Dotenv;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CT11_Fluxo_Excecao01_New_Edit_security_Rule_RuleName_Obrigatorio {
	Dotenv dotenv = Dotenv.load();
	Logger logger = LoggerFactory.getLogger(CT11_Fluxo_Excecao01_New_Edit_security_Rule_RuleName_Obrigatorio.class);
	
	@Test
	public void teste() throws InterruptedException {

		logger.info("client requested process the following list: {}", CT11_Fluxo_Excecao01_New_Edit_security_Rule_RuleName_Obrigatorio.class);
		System.setProperty("webdriver.chrome.driver",dotenv.get("PATH_WEBDRIVER_CHROME_DRIVER"));
		WebDriver driver = new ChromeDriver();
		driver.manage().window().setSize(new Dimension(1800, 1100));
		driver.get("http://homolog.core.fleetdesk.com.br/login");		    
		System.out.print(driver.getTitle());
		logger.debug("Starting process");

		
		driver.findElement(By.id("texUsuario")).click();
	    driver.findElement(By.id("texUsuario")).sendKeys("homolog@fleetdesk.com.br");
	    Thread.sleep(2000);
	    driver.findElement(By.id("pwdSenha")).click();
	    driver.findElement(By.id("pwdSenha")).sendKeys("hml123");
	    Thread.sleep(2000);
	    driver.findElement(By.id("btnLogin")).click();
	    Thread.sleep(3000);
	    driver.findElement(By.xpath("/html[1]/body[1]/div[2]/nav[1]/ul[1]/li[3]/ul[1]/li[1]/a[1]")).click();
	    Thread.sleep(3000);
	    driver.findElement(By.linkText("FleetMate")).click(); 
	    //a[contains(text(),'FleetMate')]
	    Thread.sleep(4000);	
	    driver.switchTo().frame(driver.findElement(By.xpath("/html[1]/body[1]/div[2]/div[1]/div[10]/div[1]/div[1]/div[14]/iframe[1]")));
	    driver.findElement(By.id("nav-profile-tab")).click();	    
	    driver.findElement(By.xpath("/html[1]/body[1]/div[2]/div[3]/div[1]/button[1]/i[1]")).click();
	    Thread.sleep(2000);
	    //driver.findElement(By.id("name")).click();
	    //driver.findElement(By.id("name")).sendKeys("Teste123");
	    driver.findElement(By.xpath("//input[@id='rule_for_sensors']")).click();
	    driver.findElement(By.xpath("//input[@id='rule_for_drivers']")).click();	    
	    driver.findElement(By.xpath("//input[@id='ACTUATOR_FUEL_BLOCK']")).click();
	    Thread.sleep(2000);
	    /*driver.findElement(By.xpath("//body[1]/div[2]/div[2]/form[1]/div[1]/div[7]/div[1]/span[1]/span[1]/span[1]")).click();
	    Thread.sleep(2000);
	    driver.findElement(By.xpath("//body[1]/div[2]/div[2]/form[1]/div[1]/div[7]/div[1]/span[1]/span[1]/span[1]")).sendKeys("Teste");
	    //driver.findElement(By.xpath("(//button[contains(text(),'Save')])")).click();*/
	    
	    
	    //driver.findElement(By.xpath("//form[@id='mainForm']/div/div[7]/div/span/span/span")).click();
	    //driver.findElement(By.xpath("//body[1]/div[2]/div[2]/form[1]/div[1]/div[7]/div[1]/span[1]/span[1]/span[1]")).sendKeys("Teste");
	    
	    
	    
	  
	    
	    Thread.sleep(3000);
		driver.quit();
		
	} 

}
