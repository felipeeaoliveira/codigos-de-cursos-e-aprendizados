package com.example.tests;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class BotOResumo {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "http://192.168.3.89:8680/admin-jsf/index.xhtml?windowId=259";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testBotOResumo() throws Exception {
    driver.get(baseUrl + "/admin-jsf/faces/moduloAtivosInvestimento/manterAtivoInvestimento/ativosInvestimentoVisualizar2.xhtml?windowId=724");
    driver.findElement(By.linkText("Monitoramento")).click();
    driver.findElement(By.linkText("Demonstrações Financeiras")).click();
    driver.findElement(By.id("formVisualizar:tabMonitoramentos:btnNovaDemonstracaoFinanceira")).click();
    driver.findElement(By.xpath("//div[@id='formVisualizar:tabMonitoramentos:dlgDemonstracaoFinanceira']/div/a/span")).click();
    driver.findElement(By.id("loginForm:password")).clear();
    driver.findElement(By.id("loginForm:password")).sendKeys("Caju@2511");
    driver.findElement(By.id("loginForm:username")).clear();
    driver.findElement(By.id("loginForm:username")).sendKeys("felipe.araujo");
    driver.findElement(By.id("loginForm:btnLogin")).click();
    driver.findElement(By.id("tabMonitoramentos:formMonitoramentos:j_idt378")).click();
    driver.findElement(By.id("tabMonitoramentos:formDadosGerais:btnEditarCreditoPrivado")).click();
    driver.findElement(By.cssSelector("span.ui-icon.ui-icon-triangle-1-e")).click();
    assertTrue(isElementPresent(By.cssSelector("html")));
    assertTrue(isElementPresent(By.cssSelector("body")));
    driver.findElement(By.cssSelector("div.ui-editor-button")).click();
    driver.findElement(By.xpath("//div[@id='tabMonitoramentos:formMonitoramentos:accordionPanelInfo:monitoramentoResumoEditor']/div/div/div[4]/div[4]")).click();
    driver.findElement(By.cssSelector("label > textarea")).clear();
    driver.findElement(By.cssSelector("label > textarea")).sendKeys("felipe");
    driver.findElement(By.cssSelector("input[type=\"button\"]")).click();

    div[@id='formVisualizar:tabMonitoramentos:j_idt353:monitoramentoResumoEditor']/div/div/div[4]/div[4]
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
