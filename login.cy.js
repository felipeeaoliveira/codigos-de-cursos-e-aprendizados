/// <reference types="cypress"/>


//feature
describe("Login", () => {

//cenários de login
it('Login com sucesso', () => {
    //Acessar tela de login
    cy.visit('/login')

    //Escrever campo email
    cy.get('#user').type('felipe.olievira7n@gmail.com')

    //Escrever no campo senha
    cy.get('#password').type('123456')

    //Clicar em login
    cy.get('#btnLogin').click()

    //Validar se o login foi realizado com sucesso
    cy.url().should('include', '/my-account')
})

// cenário tela principal
it('Acessar NEWS', () => {
    //NEWSLETTER clicando com ENTER
    cy.visit('/')
    cy.get('.form-control').type('felipe.olievira7n@gmail.com{Enter}')

})

//Realizar novo cadastro
it('Acessar pagina de cadastro', () => {
    
    cy.visit('/')
    cy.get('.fa-lock').click()
    //cy.get('.right_list_fix > :nth-child(2) > a').click()

//Preencher dados de cadastro
    //Escrever Nome
    cy.get('#user').type('felipe tester')

    //Escrever email
    cy.get('#email').type('felipetester@gmail.com')

    //Escrever email
    cy.get('#password').type('123456')

    //Clicar no botão cadastrar
    cy.get('#btnRegister').click()
})

})

